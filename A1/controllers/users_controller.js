//============================== User Controlller ==============================
var jsonObj={};
//load favs.json 
function load_data(){
var fs = require("fs");
fs.readFile(__dirname + "/favs.json", 'utf8', function(err, data){
        if(err){
            console.log('Error: ' + err);
            return;
        }
        else if (!data){
            console.log('File favs.json not found!');
            return;
        }
        else{
            //jsonObj = JSON.parse(data);
             set_data(JSON.parse(data));
            return;
        }
    });
}
//helper method to save the JSON object
function set_data (data){
    jsonObj=data;
}
//get specific user base on screen_name
function get_user(username){
    for (var i = 0, len = jsonObj.length; i < len; ++i) {
       if (username == jsonObj[i].user.screen_name){
           return jsonObj[i].user;
       }
    }
}
//get list of user in favs.json
function get_users(){
    var users=[];
    for (var i = 0, len = jsonObj.length; i < len; ++i) {
       users.push(jsonObj[i].user); 
    }
    return users;
}
module.exports = function(params){
    load_data();
    return {
        //GET /users
        index: function(){
           console.log(get_users());
        },
        //GET /users/index
        show: function(){
           console.log(get_user(params.id));
        }
    }
}