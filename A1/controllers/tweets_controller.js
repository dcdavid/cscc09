//============================== tweet Controlller ==============================
var jsonObj={};
//load favs.json 
function load_data(){
var fs = require("fs");
fs.readFile(__dirname + "/favs.json", 'utf8', function(err, data){
        if(err){
            console.log('Error: ' + err);
            return;
        }
        else if (!data){
            console.log('File favs.json not found!');
            return;
        }
        else{
            //jsonObj = JSON.parse(data);
             set_data(JSON.parse(data));
            return;
        }
    });
}
//helper method to save the JSON object
function set_data (data){
    jsonObj=data;
}
//get specific tweet base on screen_name
function get_tweet(tweet_id){
    for (var i = 0, len = jsonObj.length; i < len; ++i) {
       if (tweet_id == jsonObj[i].id){
           return jsonObj[i];
       }
    }
}
//get list of tweet in favs.json
function get_tweets(){
    var tweets=[];
    for (var i = 0, len = jsonObj.length; i < len; ++i) {
       tweets.push(jsonObj[i]); 
    }
    return tweets;
}
module.exports = function(params){
    load_data();
    return {
        //GET /tweets
        index: function(){
           console.log(get_tweets());
        },
        //GET /tweets/index
        show: function(){
           console.log(get_tweet(params.id));
        }
    }
}