/**
 * CSCC09 - Assignment 1 - My favourite Things
 * Node.js web server
 * Twitter clone - MumbleBee
 */

var server = process.env.IP,
        port = process.env.PORT,
        qs = require('querystring'),
        http = require('http'),
        path = require('path'),
        url = require('url'),
        fs = require('fs'),
        validTypes = {
                ".html" : "text/html",
                ".json" : "application/json",
                ".js"   : "application/javascript", 
                ".ico"  : "image/x-icon",
                ".css"  : "text/css",
                ".txt"  : "text/plain",
                ".jpg"  : "image/jpeg",
                ".gif"  : "image/gif",
                ".png"  : "image/png",
        };
        
var ID, JSONobj, JSONresp;

console.log('Server running at ' + server + port + '/');

http.createServer(function(request, response) {
        //Load JSON into variable
        fs.readFile(__dirname + "/favs.json", 'utf8', function(err, data){
            if(err){
                console.log('Error: ' + err);
                return;
            }
            else if (!data){
                console.log('File favs.json not found!');
                return;
            }
            else{
                JSONobj = JSON.parse(data);
            }
        });

        var pathname = url.parse(request.url).pathname;
        var filename = request.url;
        var query = request.url.split("/");
        query = query.slice(1, query.length);
        
        if (pathname === "/") {
        filename = "/index.html";
        loadFile(filename, response);
        }
        else {
        if (query !== undefined) {
            JSONresp = parseURL(query, response);
            if (JSONresp == null) {
                loadFile(filename, response);
            }
            else {
                response.setHeader("Content-Type", "application/json");
                                response.statusCode = 200;
                                response.end(JSON.stringify(JSONresp));
            }
        }
        }

}).listen(port);

function inArray(obj, arr) {
    for (var i=0; i < arr.length; i++) {
        if (arr[i].id === obj) return true;
    }
    return false;
}

function pushtoJSON(query) {
    var obj = [];
    for (var i=0; i < JSONobj.length; i++) {
        var t = {
                        'id': JSONobj[i].id_str,
                        'profile_image_url': JSONobj[i].user.profile_image_url,
                        'name': JSONobj[i].user.name,
                        'screen_name': JSONobj[i].user.screen_name,
                        'text': JSONobj[i].text,
                        'created_at': JSONobj[i].created_at,
                        'via': JSONobj[i].source,
                        'location': JSONobj[i].user.location,
                        'description': JSONobj[i].user.description
                }
        if(query == "users") {
            if(!inArray(t.id, obj)) {
                obj.push(t);
            }
        }
        else {
            obj.push(t);
        }
    }
    return obj;
}

// If query type is for known type of file, return null - otherwise return JSON
function parseURL(query, response) {
        var regex = /\.([0-9a-z]+)(?:[\?#]|$)/i;
        if (!String(query).match(regex)) {
                if (query.length == 1) {                        
                        if (query[0] === "tweets" || query[0] === "users") {
                                JSONresp = pushtoJSON(query[0]);
                                return JSONresp;
                        }
                        else { //by screenname
                                console.log("SCREENNAME:" + query);
                                for (var i=0; i < JSONobj.length; i++) {
                                        if (JSONobj[i].user.screen_name == query[0]) {
                                                JSONresp = JSONobj[i];
                                                return JSONresp;
                                        }
                                }
                        }               
                }
        }
        return null;
}

function loadFile(filename, response) {
        var ext = path.extname(filename);
        var currDir = __dirname;
        if (validTypes[ext]) {
                currDir += filename;
                fs.exists(currDir, function(exists) {
                        if(exists) {
                                console.log("Serving file: " + currDir);
                                showFile(currDir, response, ext);
                        } else {
                                console.log("File not found: " + currDir);
                                response.writeHead(404);
                                response.end();
                        }
                }); 
        }
        else {
                response.writeHead(404);
                response.end('These aren\'t the droids you\'re looking for...');
        }       
}

function showFile(currDir, response, mimeType) {
        fs.readFile(currDir, function(err, contents) {
                if(!err) {                      
                        response.setHeader("Content-Length", contents.length);
                        response.setHeader("Content-Type", mimeType);
                        response.statusCode = 200;
                        response.end(contents);
                } else {
                        response.writeHead(500);
                        response.end();
                }
        });
}