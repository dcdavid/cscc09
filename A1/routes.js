var root = "tweets_index_path"
var params = {};

exports.route = function (uri,method,fs){
	var query = uri.split("/")
	var controller = query[1];
	if (query.length > 1) {
		if (query[2] !== ''){
			params ['id']=query[2];
		}
	} 
	CRUD.GET(uri,controller,fs);
}

function get_route(uri,controller,fn,method){
	console.log("Started "+method+" "+uri+" at "+new Date()+"\nProcessing by "+controller+"Controller#"+fn+"\n Parameters: "+ JSON.stringify(params));
	//need to check if ctrl exist
	var ctrl= require('./controllers/'+controller+'_controller.js')(params);
	ctrl[fn](function(data){
		console.log(data);
	});
}
var CRUD ={
	GET: function(uri,controller){
		if (params.id !== undefined){
			get_route(uri,controller,'show','GET')
		}else{
			get_route(uri,controller,'index','GET')
		}
	}
}